
jQuery(document).ready(function() {
    if(jQuery(window).width() <= 425) {

        jQuery(".bloco").removeClass("no-float");

        jQuery(".main-menu-flex").removeClass("main-menu-flex");
        jQuery(".main-menu-wrapper").removeClass("main-menu-wrapper");
    }
});

jQuery(window).resize(function(){
    if(jQuery(window).width() <= 425) {

        jQuery(".bloco").removeClass("no-float");

        jQuery(".main-menu-flex").removeClass("main-menu-flex");
        jQuery(".main-menu-wrapper").removeClass("main-menu-wrapper");
        console.log(jQuery(window).width());
    }
});

jQuery(window).load(function(){
    if(jQuery(window).width() <= 425) {
        jQuery(".main-menu-flex").removeClass("main-menu-flex");
        jQuery(".main-menu-wrapper").removeClass("main-menu-wrapper");
    }
});

jQuery(document).ready(function(){
    jQuery("button.btn-navbar").click(function(){
        if(jQuery(".navbar-collapse").hasClass("in")){
            jQuery("button.btn-navbar").css({ "background-color":"#b6c931","border":"none" });
            jQuery("#menu-button").removeClass("fa-times")
            jQuery("#menu-button").addClass("fa-bars");
        } else {
            jQuery("button.btn-navbar").css({ "background-color":"#225f27","border":"none" });
            jQuery("#menu-button").removeClass("fa-bars");
            jQuery("#menu-button").addClass("fa-times");
        }
    });
});

jQuery(document).ready(function(){
    jQuery(".social-media>ul>li").mouseenter(function(){
        jQuery(this).addClass("animated bounceIn");
    });
    jQuery(".social-media").mouseleave(function(){
        jQuery(".social-media>ul>li").removeClass("animated bounceIn");
    });

});

jQuery(document).ready(function(){
    
    // jQuery(".tab-description img").each(function(){
    //     console.log(jQuery(this));
    //     jQuery(this).addClass("img-responsive");
    // });

    jQuery("img").addClass("img-responsive");

    jQuery(".panel-heading").click(function(){
        
        console.log(jQuery(this).parent().attr('aria-expanded'));

        if (jQuery(this).parent().attr('aria-expanded') === "true") {
            jQuery(".fa", this).addClass("rotate-icon");
        } else {
            jQuery(".fa", this).removeClass("rotate-icon");
        }
    
    });

});

jQuery(window).scroll(function(){
    var descPx = jQuery(window).scrollTop();
    if(descPx > 160 ){
        jQuery(".mobile-top-menu").addClass("mobile-top-menu-scroll");
    } else if (descPx <= 160) {
        jQuery(".mobile-top-menu").removeClass("mobile-top-menu-scroll");
    }
});

$(document).ready(function (){
    $("#contact-button").click(function (){
        $('html, body').animate({
            scrollTop: $("#contact_form").offset().top
        }, 1000);
    });
});

// jQuery(document).ready(function(){
//     jQuery("#solutions-menu").click(function(){
//          if (jQuery("#solutions-menu>a>i").hasClass("fa-caret-down")){
//             jQuery("#solutions-menu>a>i").removeClass("fa-caret-down");
//             jQuery("#solutions-menu>a>i").addClass("fa-caret-up");
//          } else {
//             jQuery("#solutions-menu>a>i").removeClass("fa-caret-up");
//             jQuery("#solutions-menu>a>i").addClass("fa-caret-down");
//          }
//     });
// });


jQuery(document).ready(function(){

    jQuery("li.dropdown").mouseover(function(){
        $(this).addClass("open");
    });

    jQuery("li.dropdown").mouseout(function(){
        $(this).removeClass("open");
    });


});

jQuery(document).ready(function(){
    jQuery("#solicitar-orcamento").click(function(){
        $('html, body').animate({
            scrollTop: $("#contact_form").offset().top
        }, 1000);
    });
});