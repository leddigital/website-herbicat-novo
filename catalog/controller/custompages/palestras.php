<?php
class ControllerCustompagesPalestras extends Controller
{
	public function index()
	{

		$this->document->setTitle('Palestras | Herbicat');


		$this->load->model('catalog/palestras');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$page = 1;
		
		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		}
		
		$posts = $this->model_catalog_palestras->getNews($page);
		//$product_info = $this->model_catalog_product->getProducts();
		$post_number = $this->model_catalog_palestras->getPaginationNews();

		$num_post = $post_number->row['num_post'];
		
		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		$url = "";

		$pagination = new Pagination();
		$pagination->total = $num_post;
		$pagination->page = $page;
		$pagination->limit = 6;
		$pagination->url = $this->url->link('custompages/palestras', $url . '&page={page}');


		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['search'] = $this->load->controller('common/search');
		$data['base'] = $server;
		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($num_post) ? (($page - 1) * 6) + 1 : 0, ((($page - 1) * 6) > ($num_post - 6)) ? $num_post : ((($page - 1) * 6) + 6), $num_post, ceil($num_post / 6));


		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('<i class="fa fa-home" aria-hidden="true"></i>'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Palestras',
			'href' => $this->url->link('custompages/palestras')
		);

		foreach ($posts as $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], 500, 300);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', 500, 250);
			}

			$data['posts'][] = array(
				'title' 				=> 			$result['title'],
				'image' 				=> 			$image,
				'short_description' 	=> 			$result['short_description'],
				'tag' 					=> 			$result['tag'],		
				'post_id'				=>			$result['post_id'],
				'date_published'		=>			$result['date_published'],
				'meta_title'			=>			$result['meta_title'],
				'prev_route'			=> 			$this->request->get['route']
			);
		}

		$this->response->setOutput($this->load->view('custompages/palestras', $data));
	}
}
