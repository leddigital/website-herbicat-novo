<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class ControllerCustompagesSender extends Controller
{
	public function index()
	{
        // Require PHPMAILER
        require (DIR_SYSTEM . 'library/PHPMailer-FE_v4.11/src/Exception.php');
        require (DIR_SYSTEM . 'library/PHPMailer-FE_v4.11/src/PHPMailer.php');
        require (DIR_SYSTEM . 'library/PHPMailer-FE_v4.11/src/SMTP.php');

        $data['info'][] = array (
            'name'      =>      $_POST['name'],
            'business'  =>      $_POST['business'],
            'email'     =>      $_POST['email'],
            'tel'       =>      $_POST['tel'],
            'message'   =>      $_POST['message']
        );

        $this->response->setOutput($this->load->view('custompages/sender', $data));

        $mail = new PHPMailer(true);

        try {
            
            $mail->CharSet = 'UTF-8';
            //$mail->SMTPDebug = 2;                                            
            $mail->isSMTP();                                                   
            $mail->Host = "mail.alfaagropecas.com.br";                      
            $mail->SMTPAuth = true;
            $mail->Username = "teste@alfaagropecas.com.br";
            $mail->Password = "teste@alfaagro";
            $mail->Port = 587;
            $mail->SMTPSecure = false;
            $mail->SMTPAutoTLS = false;
            
            $mail->setFrom('teste@alfaagropecas.com.br', 'Contato - Herbicat');
            $mail->addAddress('edinaldoprofi99@gmail.com', 'Contato - Herbicat');
            $mail->addBCC('edinaldo@agencialed.com.br', 'Contato - Herbicat | Cópia');
            $mail->isHTML(true);
            $mail->Subject = "Contato Site - Herbicat.com.br";
            //$mail->MsgHTML('OLAR');
            $mail->MsgHTML($this->load->view('custompages/sender', $data));
            $mail->send();

        } catch (Exception $e) {
            echo $mail->ErrorInfo;
        }
        catch (\Exception $e){
            echo $e->getMessage();
        }
    }
}

