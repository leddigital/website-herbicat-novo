<?php
class ControllerCustompagesSinglepost extends Controller
{
	public function index()
	{

		$this->load->model('catalog/singlepost');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');


		$query = $this->model_catalog_singlepost->getSinglePost($_GET['id']);
		$popular_products = $this->model_catalog_product->getPopularProducts(3);

		$this->document->setTitle($query->row['title'] . ' | Herbicat');
		
		// Adicionar quando os melhores produtos forem definidos
		//$best_seller_products = $this->model_catalog_product->getBestSellerProducts(6);

		// Adcionado temporariamente em substituição da consulta acima
		$best_seller_products = $this->model_catalog_product->getPopularProducts(6);
		
		$latest_posts = $this->model_catalog_singlepost->getLatestPosts($_GET['id']);

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}



		$data['atual_url'] = 'http://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['search'] = $this->load->controller('common/search');
		$data['base'] = $server;

		if ($query->row['image']) {
			$image = $this->model_tool_image->resize($query->row['image'], 1280, 720);
		} else {
			$image = $this->model_tool_image->resize('placeholder.png', 500, 250);
		}

		$data['post'] = array(
			'title' 				=> 			$query->row['title'],
			'date_published'		=>			$query->row['date_published'],
			'image' 				=> 			$image,
			'short_description' 	=> 			$query->row['short_description'],
			'description'			=>			utf8_decode($query->row['description']),
			'tag' 					=> 			$query->row['tag'],
			'meta_title'			=>			$query->row['meta_title'],
			'category_id'			=>			$query->row['category_id']
		);

		foreach ($best_seller_products as $result){
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], 500, 300);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', 500, 250);
			}

			$data['best_products'][] = array(
				'name' 					=> 			$result['name'],
				'image' 				=> 			$image,
				'price' 				=> 			$result['price'],
				'description' 			=> 			$result['description'],		
				'product_id'			=>			$result['product_id'],
				'meta_title'			=>			$result['meta_title']
			);
		}
		
		foreach ($popular_products as $result) {
			
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], 500, 300);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', 500, 250);
			}

			$data['popular_products'][] = array(
				'name' 					=> 			$result['name'],
				'image' 				=> 			$image,
				'price' 				=> 			$result['price'],
				'description' 			=> 			$result['description'],		
				'product_id'			=>			$result['product_id'],
				'meta_title'			=>			$result['meta_title']
			);
		}

		foreach ($latest_posts as $result) {
			
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], 500, 250);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', 500, 250);
			}
			
			$data['latest_posts'][] = array (
				'post_id'				=>			$result['post_id'],
				'image' 				=> 			$image,
				'title'					=>			$result['title'],
				'viewed'				=>			$result['viewed'],
				'short_description'		=>			$result['short_description']
			);
		}

		
		
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('<i class="fa fa-home" aria-hidden="true"></i>'),
			'href' => $this->url->link('common/home')
		);

		
		if ($data['post']['category_id'] == 9) {
			
			$data['breadcrumbs'][] = array(
				'text' => 'Palestras',
				'href' => $this->url->link('custompages/palestras')
			);

		} else if ($data['post']['category_id'] == 13) {
			
			$data['breadcrumbs'][] = array(
				'text' => 'Feiras e eventos',
				'href' => $this->url->link('custompages/eventos')
			);

		} else if ($data['post']['category_id'] == 11) {

			$data['breadcrumbs'][] = array(
				'text' => 'Artigos',
				'href' => $this->url->link('custompages/artigos')
			);

		} else if ($data['post']['category_id'] == 15) {

			$data['breadcrumbs'][] = array(
				'text' => 'Catálogos',
				'href' => $this->url->link('custompages/manuals')
			);

		} else {
			
			$data['breadcrumbs'][] = array(
				'text' => 'Notícias e Artigos',
				'href' => $this->url->link('custompages/news')
			);

		}
		
		$data['breadcrumbs'][] = array(
			'text' => $query->row['title'],
			'href' => $this->url->link('custompages/singlepost&id=' . $_GET['id'])
		);

		$this->response->setOutput($this->load->view('custompages/singlepost', $data));
	}
}
