<?php
class ControllerCustompagesHerbicat extends Controller
{
	public function index()
	{

		$this->load->model('catalog/information');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);


		$information_id = 4;
		$information_info = $this->model_catalog_information->getInformation($information_id);


		$this->document->setTitle($information_info['meta_title']);
		$this->document->setDescription($information_info['meta_description']);
		$this->document->setKeywords($information_info['meta_keyword']);

		$data['breadcrumbs'][] = array(
			'text' => $information_info['title'],
			'href' => $this->url->link('information/information', 'information_id=' .  $information_id)
		);

		$data['heading_title'] = $information_info['title'];

		$data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('custompages/herbicat', $data));
	}
}
