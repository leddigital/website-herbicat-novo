<?php
class ModelCatalogManuals extends Model {

	public function getPaginationNews(){

		$post_number = $this->db->query("SELECT count(category_id) AS num_post FROM oc_bm_post_to_category WHERE category_id = 15;");
		return $post_number;
	}

	public function getNews($page) {

		$posts = $this->getPaginationNews();
		$post_number = $posts->row['num_post'];

		$limit = 6;
		$offset = (6*$page)-$limit;
		
		$sql = "SELECT
		ps.title, ps.short_description,p.date_published, p.image, p.post_id, ps.tag	
		FROM herbicat.oc_bm_post as p
		inner join herbicat.oc_bm_post_description as ps on p.post_id = ps.post_id
		inner join herbicat.oc_bm_post_to_category as pcc on pcc.post_id = p.post_id
		where pcc.category_id = 15
		order by p.post_id ASC

		LIMIT ".$limit." OFFSET ".$offset;
		$query = $this->db->query($sql);
		
		return $query->rows;
	}	
}
